### Caesar Cipher Online 

This is a caesar cipher implemented using javascript. The user enters a string and shifting number, then
the script will output the corresponding encrypted string.

An image of a cipher is included as well.